%% Transformada R�pida de Fourier y su Inversa

close all
% Frecuencia de muestreo de 11025
% audio = audiorecorder(11025,16,1); %Configuraci�n de toma de muestras de audio
% record(audio,15); %Empieza a tomar audio
% stop(audio); %Detiene la toma de muestras de audio
% f = getaudiodata(audio); %En f se guarda el vector de datos de audio
% plot(f)
% sound(f,Fs)
disp('La frecuencia de muestreo es: ')
disp(Fs)
figure(1)
plot(f,'m'); grid on;
title('Audio adquirido');
xlabel('Muestras','FontSize',10); 
ylabel('Amplitud','FontSize',10); 

%% Transformada R�pida de Fourier 
i=sqrt(-1);
[M,N]=size(f);

for u=1:M
  for v=1:N;
      Fourier_T(u,v)=0;
      for x=1:M
          for y=1:N,
              Fourier_T(u,v) = Fourier_T(u,v) + f(x,y)*exp(-2*pi*i*((u-1)*(x-1)/M+(v-1)*(y-1)/N));
          end
      end
  end
end

F_FFT = abs(Fourier_T);
figure(2)
plot(F_FFT); grid on;
title('Transformada R�pida de Fourier');
xlabel('Frecuencia [Hz]','FontSize',10); 
ylabel('|FFT(x[n])|','FontSize',10); 


%% Transformada R�pida Inversa de Fourier 
i=sqrt(-1);
[M,N]=size(f);

for x=1:M
  for y=1:N;
      fourier_inv(u,v)=0;
      for u=1:M
        for v=1:N;
            fourier_inv(x,y) = fourier_inv(x,y) + Fourier_T(u,v)*exp(2*pi*i*((u-1)*(x-1)/M+(v-1)*(y-1)/N));
        end
      end
  end
end
figure(3)
fourier_inv = (1/M*N)*fourier_inv;
fourier_inv = real(fourier_inv);
plot(fourier_inv); grid on;
title('Transformada R�pida Inversa de Fourier');
xlabel('Muestras','FontSize',10); 
ylabel('|IFFT(x[n])|','FontSize',10); 

%% Crear los .wav
filename_f = 'audio_adquirido.wav';
filename_iftt = 'audio_reconstruido.wav';
audiowrite(filename_f,f,Fs);
audiowrite(filename_ifft,fourier_inv,Fs);